﻿using Z.Ddd.Common.DomainServiceRegister.Domain;

namespace Z.SunBlog.Core.MenuModule.DomainManager
{
    public interface IMenuManager : IBusinessDomainService<Menu>
    {
    }
}
