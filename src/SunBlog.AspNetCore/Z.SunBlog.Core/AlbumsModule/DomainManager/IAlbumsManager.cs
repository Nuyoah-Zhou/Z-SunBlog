﻿using Z.Ddd.Common.DomainServiceRegister.Domain;

namespace Z.SunBlog.Core.AlbumsModule.DomainManager
{
    public interface IAlbumsManager : IBusinessDomainService<Albums>
    {
    }
}
