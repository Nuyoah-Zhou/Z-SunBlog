﻿using Z.Ddd.Common.DomainServiceRegister.Domain;

namespace Z.SunBlog.Core.PraiseModule.DomainManager
{
    public interface IPraiseManager : IBusinessDomainService<Praise>
    {
    }
}
