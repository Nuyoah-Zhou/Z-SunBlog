﻿using Z.Ddd.Common.DomainServiceRegister.Domain;

namespace Z.SunBlog.Core.ArticleTagModule.DomainManager
{
    public interface IArticleTagManager : IBusinessDomainService<ArticleTag>
    {
    }
}
