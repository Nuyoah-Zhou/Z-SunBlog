﻿using Z.Ddd.Common.DomainServiceRegister.Domain;
using Z.SunBlog.Core.TagModule;

namespace Z.SunBlog.Core.TagModule.DomainManager
{
    public interface ITagsManager : IBusinessDomainService<Tags>
    {
    }
}
