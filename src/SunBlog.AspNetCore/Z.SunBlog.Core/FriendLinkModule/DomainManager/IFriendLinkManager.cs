﻿using Z.Ddd.Common.DomainServiceRegister.Domain;

namespace Z.SunBlog.Core.FriendLinkModule.DomainManager
{
    public interface IFriendLinkManager : IBusinessDomainService<FriendLink>
    {
    }
}
