﻿using Z.Ddd.Common.DomainServiceRegister.Domain;
using Z.SunBlog.Core.TagModule;
using Z.SunBlog.Core.TalksModule;

namespace Z.SunBlog.Core.TalksModule.DomainManager
{
    public interface ITalksManager : IBusinessDomainService<Talks>
    {
    }
}
