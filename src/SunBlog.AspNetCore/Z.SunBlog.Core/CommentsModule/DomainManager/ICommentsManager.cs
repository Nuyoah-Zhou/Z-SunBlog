﻿using Z.Ddd.Common.DomainServiceRegister.Domain;


namespace Z.SunBlog.Core.CommentsModule.DomainManager
{
    public interface ICommentsManager : IBusinessDomainService<Comments>
    {
    }
}
