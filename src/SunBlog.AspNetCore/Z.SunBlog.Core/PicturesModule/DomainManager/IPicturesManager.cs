﻿using Z.Ddd.Common.DomainServiceRegister.Domain;

namespace Z.SunBlog.Core.PicturesModule.DomainManager
{
    public interface IPicturesManager : IBusinessDomainService<Pictures>
    {
    }
}
