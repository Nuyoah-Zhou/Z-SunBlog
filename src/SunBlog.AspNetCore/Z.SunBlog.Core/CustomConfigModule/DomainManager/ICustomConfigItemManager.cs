﻿using Z.Ddd.Common.DomainServiceRegister.Domain;


namespace Z.SunBlog.Core.CustomConfigModule.DomainManager
{
    public interface ICustomConfigItemManager : IBusinessDomainService<CustomConfigItem>
    {
    }
}
