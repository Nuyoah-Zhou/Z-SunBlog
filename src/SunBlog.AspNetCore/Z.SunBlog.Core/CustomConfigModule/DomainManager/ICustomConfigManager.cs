﻿using Z.Ddd.Common.DomainServiceRegister.Domain;


namespace Z.SunBlog.Core.CustomConfigModule.DomainManager
{
    public interface ICustomConfigManager : IBusinessDomainService<CustomConfig>
    {
    }
}
